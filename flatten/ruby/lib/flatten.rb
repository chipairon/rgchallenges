# Different implementations for solving the flattenning challenge for arrays.
# flatten([1, [2, [3]], [4]]) => [1, 2, 3, 4]
module Flatten

  # Flattens an array of arrays into a one dimensional array.
  # Preserves the entry_array.
  def self.flatten entry_array
    return entry_array unless entry_array.is_a? Array
    out = []
    entry_array.map do |element|
      if element.is_a? Array
        # recursive call, for every element that could be an array
        flatten(element).each {|el| out << el}
      else
        out << element
      end
      out
    end
    out
  end

  # Flattens an array of arrays into a one dimensional array.
  # The entry_array is modified in the process, so it won't be
  # usable afterwards. If that is not a problem, this will be
  # faster than the implementation on the standard ruby libray
  # or the non-destructive method #flatten
  def self.flatten! entry_array
    return entry_array unless entry_array.is_a? Array
    out = []
    pending = entry_array
    while !pending.empty? # NOTE: cannot be used #any? or [nil].any? will not behave
      item = pending.shift
      if item.is_a? Array
        item.reverse_each do |item_sub_element|
          pending.unshift item_sub_element
        end
      else
        out << item
      end
    end
    out
  end
end
