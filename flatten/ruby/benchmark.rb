require 'benchmark/ips'
require_relative 'lib/flatten'

def run_benchmark(entry_array)
  Benchmark.ips do |x|
    x.report("#flatten") {
      Flatten.flatten entry_array
    }
    x.report("#flatten!") {
      Flatten.flatten! entry_array
    }
    x.compare!
  end
end

puts "**************************************************"
puts "* Benchmark for an input with moderated nesting: *"

sample = [[1], 1, [2, [3, 3, [4, 4], 3], 2], 1, [[3], 2]]
ARRAY_1 = []
1000.times do
  ARRAY_1 << sample * 10
end

run_benchmark(ARRAY_1)


puts "*********************************************"
puts "* Benchmark for an input with deep nesting: *"

ARRAY_2 = []
1000.times do
  sample = [[1], 1, [2, [3, 3, [4, 4], 3], 2], 1, [[3], 2]]
  row = [] + sample
  99.times do
    row[0] = row.dup
  end
  ARRAY_2 << row
end

run_benchmark(ARRAY_2)

puts "*******************************************"
puts "* Benchmark for an input with NO nesting: *"

sample = [1, 1, 2, 3, 3, 4, 4, 3, 2, 1, 3, 2]
ARRAY_3 = []
1000.times do
  ARRAY_3 << sample * 10
end

run_benchmark(ARRAY_3)
