require "minitest/autorun"
require_relative "../lib/flatten"

module TheTests
  def test_does_not_modify_input_if_is_not_an_array
    assert_equal nil, flatten(nil)
    assert_equal "a", flatten("a")
    assert_equal 3, flatten(3)
  end

  def test_respects_nils
    assert_equal [nil, nil, nil], flatten([[nil, [nil]], nil])
  end

  def test_edge_cases_of_already_flattened_inputs
    assert_equal [], flatten([])
    assert_equal [2], flatten([2])
    assert_equal [nil], flatten([nil])
    assert_equal [1, 2], flatten([1, 2])
  end

  def test_flattens
    assert_equal [1, 2], flatten([1, [2]])
    assert_equal [2, 3, 4, 5], flatten([2,[[3], 4], 5])
    assert_equal [2, 3, 4, 5], flatten([2,[3, [4]], 5])
    assert_equal [1, 2, 3, 4, 5], flatten([1, [2], [[3], 4], 5])
    assert_equal [], flatten([[[], []]])
  end
end

class TestFlattenNonDestructive < Minitest::Test
  def flatten x
    Flatten.flatten x
  end

  include TheTests

  def test_entry_is_not_mutated
    entry = [2,[[3], 4], 5].freeze

    flatten(entry)

    assert entry.frozen?
  end
end

class TestFlattenDestructive < Minitest::Test
  def flatten x
    Flatten.flatten! x
  end

  include TheTests

  def test_confirm_entry_is_mutated
    entry = [2,[[3], 4], 5].freeze

    assert_raises RuntimeError do # Trying to mutate a frozen object raises RuntimeError
      flatten(entry)
    end

    unfrozen_entry = [2,[[3], 4], 5]
    size_before = unfrozen_entry.length

    flatten(unfrozen_entry)
    size_after = unfrozen_entry.length

    refute_equal size_before, size_after
  end
end
