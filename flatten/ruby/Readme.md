# Flatten array challenge

Different implementations for solving the flattenning challenge for arrays.

    flatten([1, [2, [3]], [4]]) => [1, 2, 3, 4]

## Tests
Tests can be run with `ruby test/flatten_test.rb`

## Benchmarks
The benchmarks can be run with `ruby benchmark.rb`

They exercise the different implementations with different inputs:

- Moderate nesting
- Deep nesting
- No nesting at all

### Benchmark results
*Benchmarking is only valuable with data resembling the real use case. The recommendations
are based on the input data analyzed and may change in a different use case.*

The most performant version is `#flatten!`, that is iterative and mutates the entry array.

`#flatten` is a version that does not mutate the entry array but is slower. It is still
preferred to `Array#flatten` as implemented in the standard ruby library, as is faster for
the inputs tried in the benchmarks.

Here is the output of the benchmark executed on Ubuntu 64 bits with Intel® Core™ i5-6400 CPU @ 2.70GHz × 4 and 8 GB of RAM, with ruby 2.3.1

    **************************************************
    * Benchmark for an input with moderated nesting: *
    Warming up --------------------------------------
                #flatten     2.000  i/100ms
               #flatten!   282.228k i/100ms
    Calculating -------------------------------------
                #flatten      3.676M (±11.3%) i/s -     12.306M in   3.675869s
               #flatten!      6.275M (± 1.8%) i/s -     31.610M in   5.039542s
    
    Comparison:
               #flatten!:  6274608.7 i/s
                #flatten:  3676187.2 i/s - 1.71x  slower
    
    *********************************************
    * Benchmark for an input with deep nesting: *
    Warming up --------------------------------------
                #flatten     1.000  i/100ms
               #flatten!   252.548k i/100ms
    Calculating -------------------------------------
                #flatten      2.729M (± 6.1%) i/s -      8.264M in   3.227143s
               #flatten!      6.601M (± 1.8%) i/s -     33.084M in   5.013712s
    
    Comparison:
               #flatten!:  6600809.1 i/s
                #flatten:  2729473.6 i/s - 2.42x  slower
    
    *******************************************
    * Benchmark for an input with NO nesting: *
    Warming up --------------------------------------
                #flatten     6.000  i/100ms
               #flatten!   281.191k i/100ms
    Calculating -------------------------------------
                #flatten      5.148M (±10.2%) i/s -     20.105M in   4.256968s
               #flatten!      6.449M (± 1.4%) i/s -     32.337M in   5.015560s
    
    Comparison:
               #flatten!:  6448540.3 i/s
                #flatten:  5148115.4 i/s - 1.25x  slower
    
