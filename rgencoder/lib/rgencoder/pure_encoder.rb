module Rgencoder
  # This module holds the functionality written in pure ruby.
  module Pure
    require 'rgencoder/pure_encoder/encoder'
    Rgencoder.Encoder = PureEncoder
  end
end
