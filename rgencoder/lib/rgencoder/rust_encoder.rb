require "helix_runtime"
begin
  require "rgencoder/native"
  Rgencoder.Encoder = RustEncoder
rescue LoadError
  warn "Unable to load rgencoder/native. Please run `rake build`"
end
