#[macro_use]
extern crate helix;

ruby! {
  class RustEncoder {
    def to_morse(input: String) -> String {
      encoder(input, 0).to_string()
    }
    def to_obfuscated_morse(input: String) -> String {
      encoder(input, 1).to_string()
    }
  }
}

/// Encodes a string of ASCII characters, numbers and spaces to
/// a new string based on the encoding table passed as parameter.
fn encoder(string: String, encoding_table_index: usize) -> String {
  let x = string.lines().map(|line| {
    line.split_whitespace().map(|word| {
      encode_word(&word, encoding_table_index)
    }).collect::<Vec<String>>().join("/")
});
  x.collect::<Vec<String>>().join("\n")
}

fn encode_word(word: &str, encoding_table_index: usize) -> String {
  let y = word.chars().map(|chr| {
    morse_table(chr as i32)[encoding_table_index]
  });
  y.collect::<Vec<&'static str>>().join("|")
}

fn morse_table(char_code: i32) -> [&'static str; 2] {
  match make_uppercase(char_code) {
    65 => [".-", "1A"],         // A
    66 => ["-...", "A3"],       // B
    67 => ["-.-.", "A1A1"],     // C
    68 => ["-..", "A2"],        // D
    69 => [".", "1"],           // E
    70 => ["..-.", "2A1"],      // F
    71 => ["--.", "B1"],        // G
    72 => ["....", "4"],        // H
    73 => ["..", "2"],          // I
    74 => [".---", "1C"],       // J
    75 => ["-.-", "A1A"],       // K
    76 => [".-..", "1A2"],      // L
    77 => ["--", "B"],          // M
    78 => ["-.", "A1"],         // N
    79 => ["---", "C"],         // O
    80 => [".--.", "1B1"],      // P
    81 => ["--.-", "B1A"],      // Q
    82 => [".-.", "1A1"],       // R
    83 => ["...", "3"],         // S
    84 => ["-", "A"],           // T
    85 => ["..-", "2A"],        // U
    86 => ["...-", "3A"],       // V
    87 => [".--", "1B"],        // W
    88 => ["-..-", "A2A"],      // X
    89 => ["-.--", "A1B"],      // Y
    90 => ["--..", "B2"],       // Z
    48 => ["-----", "E"],       // 0
    49 => [".----", "1D"],      // 1
    50 => ["..---", "2C"],      // 2
    51 => ["...--", "3B"],      // 3
    52 => ["....-", "4A"],      // 4
    53 => [".....", "5"],       // 5
    54 => ["-....", "A4"],      // 6
    55 => ["--...", "B3"],      // 7
    56 => ["---..", "C2"],      // 8
    57 => ["----.", "D1"],      // 9
    46 => [".-.-.-", "1A1A1A"], // .
    44 => ["--..--", "B2B"],    // ,
    _ => ["?", "?"],
  }
}

fn make_uppercase(code_point: i32) -> i32 {
  if code_point >= 97 && code_point <= 122 {
      code_point - 32
  } else {
      code_point
  }
}
