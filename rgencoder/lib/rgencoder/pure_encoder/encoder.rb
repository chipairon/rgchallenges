module Rgencoder
  class PureEncoder
    MORSE_INDEX = 0
    OBFUSCATED_INDEX = 1
    MORSE_MAP = {
      65 => [".-", "1A"],         # A
      66 => ["-...", "A3"],       # B
      67 => ["-.-.", "A1A1"],     # C
      68 => ["-..", "A2"],        # D
      69 => [".", "1"],           # E
      70 => ["..-.", "2A1"],      # F
      71 => ["--.", "B1"],        # G
      72 => ["....", "4"],        # H
      73 => ["..", "2"],          # I
      74 => [".---", "1C"],       # J
      75 => ["-.-", "A1A"],       # K
      76 => [".-..", "1A2"],      # L
      77 => ["--", "B"],          # M
      78 => ["-.", "A1"],         # N
      79 => ["---", "C"],         # O
      80 => [".--.", "1B1"],      # P
      81 => ["--.-", "B1A"],      # Q
      82 => [".-.", "1A1"],       # R
      83 => ["...", "3"],         # S
      84 => ["-", "A"],           # T
      85 => ["..-", "2A"],        # U
      86 => ["...-", "3A"],       # V
      87 => [".--", "1B"],        # W
      88 => ["-..-", "A2A"],      # X
      89 => ["-.--", "A1B"],      # Y
      90 => ["--..", "B2"],       # Z
      48 => ["-----", "E"],       # 0
      49 => [".----", "1D"],      # 1
      50 => ["..---", "2C"],      # 2
      51 => ["...--", "3B"],      # 3
      52 => ["....-", "4A"],      # 4
      53 => [".....", "5"],       # 5
      54 => ["-....", "A4"],      # 6
      55 => ["--...", "B3"],      # 7
      56 => ["---..", "C2"],      # 8
      57 => ["----.", "D1"],      # 9
      46 => [".-.-.-", "1A1A1A"], # .
      44 => ["--..--", "B2B"]     # ,
    }

    # Encodes a string of ASCII characters, numbers and spaces to
    # our morse code representation
    def self.to_morse(string)
      encoder(string, MORSE_INDEX)
    end

    # Encodes a string of ASCII characters, numbers and spaces to
    # our obfuscated morse code representation
    def self.to_obfuscated_morse(string)
      encoder(string, OBFUSCATED_INDEX)
    end

    private

      # Encodes a string of ASCII characters, numbers and spaces to
      # a new string based on the encoding table passed as parameter.
      def self.encoder(string, encoding_table_index)
        return string unless string.is_a? String
        string.each_line.map do |line|
          line.split.map do |word|
            encode_word(word, encoding_table_index)
          end.join("/")
        end.join("\n")
      end

      def self.encode_word(word, encoding_table_index)
        word.codepoints.map do |code_point|
          char_translation = MORSE_MAP[upcase_codepoint(code_point)]
          if char_translation.nil?
            "?"
          else
            char_translation[encoding_table_index]
          end
        end.join("|")
      end

      # https://upload.wikimedia.org/wikipedia/commons/8/81/Table_ascii_extended.png
      def self.upcase_codepoint(code_point)
        if code_point >= 97 && code_point <= 122
          code_point - 32
        else
          code_point
        end
      end

  end
end
