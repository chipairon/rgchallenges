
require "rgencoder/version"

module Rgencoder
  def self.Encoder=(the_encoder_to_use)
    remove_const :Encoder if const_defined?(:Encoder, false)
    const_set :Encoder, the_encoder_to_use
  end

  if ENV["USE_PURE_RUBY"]
    require 'rgencoder/pure_encoder'
  else
    begin
      require 'rgencoder/rust_encoder'
    rescue LoadError
      puts "Error loading rust version, loading pure ruby instead."
      require 'rgencoder/pure_encoder'
    end
  end
end
