require 'test_helper'
module EdgeCaseTests
  def test_edge_cases
    assert_equal "", to_morse("")
    assert_equal "", to_morse("\n")
    assert_equal "", to_morse("\t\t")
  end

  def test_non_translatable_inputs_are_translated_as_?
    assert_equal "?", to_morse("ü")
    assert_equal ".|?", to_morse("e♥")
  end
end

module MorseEncodingTests
  def test_encodes_single_word
    assert_equal "-..|---|--.", to_morse("Dog")
  end

  def test_encodes_multiple_words_in_single_line
    assert_equal "../.-|--/..|-./-|.-.|---|..-|-...|.-..|.", to_morse("I AM IN TROUBLE")
  end

  def test_encodes_multiline_strings
    msg = "HELLO\nI AM IN TROUBLE"
    encoding = "....|.|.-..|.-..|---\n../.-|--/..|-./-|.-.|---|..-|-...|.-..|."

    assert_equal encoding, to_morse(msg)
  end
end

module ObfuscationTests
  def test_encodes_single_word
    assert_equal "A2|C|B1", to_obfuscated_morse("Dog")
    assert_equal "2/1A|B/2|A1/A|1A1|C|2A|A3|1A2|1", to_obfuscated_morse("I am in trouble")
  end
end

class RgencoderTest < Minitest::Test
  def to_morse x
    Rgencoder::Encoder.to_morse x
  end
  def to_obfuscated_morse x
    Rgencoder::Encoder.to_obfuscated_morse x
  end
  include EdgeCaseTests
  include MorseEncodingTests
  include ObfuscationTests
end
