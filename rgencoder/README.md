# Rgencoder

Secret encoder program: *shuuuush!*

You're competing in a battle on a far away planet and you're in some trouble.
You need to send a distress call to your home base for reinforcements, however,
enemy agents are listening. Luckily your team have a secret encoding for messages.

It's Morse code with further obfuscation.

The letters are translated to its Morse representation. A `/` character will separate each letter and a `|` character will represent the space.

The obfuscation algorithm has the following rules additional rules:

- Each '-' sequence in the Morse representation is replaced by the corresponding letter at that position in the alphabet.
Examples: '-' => 'A', '----' => 'D'
- Each '.' sequence in the Morse representation is replaced by the number of dots.
Examples: '.' => '1', '....' => '4'

For an input 'I AM IN TROUBLE', the Morse encoding will be `../.-|--/..|-./-|.-.|---|..-|-...|.-..|.` and the corresponding obfuscated code `2/1A|B/2|A1/A|1A1|C|2A|A3|1A2|1`.

## Installation

Make sure Rust is installed and added to the `PATH`:

    curl https://sh.rustup.rs -sSf | sh

If Rust is installed, running `rustc --version` should not fail.

Add this line to your application's Gemfile:

```ruby
gem 'rgencoder'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install rgencoder

## Usage

The program can read from STDIN or from files passed as parameters. It will output
the encoded text found in STDOUT.

By default it uses the obfuscated Morse encoding, but a parameter `--use-morse`
can be used to tell it to use the non-obfuscated encoding.

####Example invocations:

  rgencoder -h                  => Shows this help.

  rgencoder msg.txt             => Encodes the text found in the file msg.txt

  cat msg.txt | rgencoder       => Encodes the text found in the file msg.txt

  rgencoder --use-morse msg.txt => Encodes the text found in the file msg.txt using the non-obfuscated encoding.

## Development

This gem has two implementations, one fast made in Rust and another one (slower) made in pure Ruby. By default, it will try to install and run the Rust version, as it is significantly faster, but an environment variable `USE_PURE_RUBY` can be defined (no matter the value, just that it exists) to force the gem to use the pure Ruby implementation.

To run the tests:

    $ bundle exec rake test

To run the tests but for the pure ruby implementation:

    $ USE_PURE_RUBY=1 bundle exec rake test

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Benchmarks
For small inputs, the difference is not significant between the two implementations:

```
$ time USE_PURE_RUBY=1 bin/rgencoder test/sample_cat.txt > /dev/null

real	0m0.168s
user	0m0.152s
sys	  0m0.012s

$ time bin/rgencoder test/sample_cat.txt > /dev/null

real	0m0.160s
user	0m0.144s
sys	  0m0.012s

```

But the bigger the input is, the bigger the gains of the Rust implementation. For a test file with a few MB:

```
$ time bin/rgencoder test/huge.txt > /dev/null

real	0m1.131s
user	0m1.092s
sys	  0m0.036s

$ time USE_PURE_RUBY=1 bin/rgencoder test/huge.txt > /dev/null

real	0m9.094s
user	0m9.056s
sys	  0m0.032s
```

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
