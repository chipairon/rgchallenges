# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rgencoder/version'

Gem::Specification.new do |spec|
  spec.name          = "rgencoder"
  spec.version       = Rgencoder::VERSION
  spec.authors       = ["Chipairon"]
  spec.email         = ["Chipairon@no.email"]
  spec.summary       = "RGEncoder algorithm in Rust and pure Ruby."

  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files         = ["lib/rgencoder.rb"] 
  spec.executables   << "rgencoder"
  spec.require_paths = ["lib"]

  spec.add_dependency "helix_runtime", "~> 0.6.1"
  spec.add_dependency "thor", "~> 0.19"

  spec.add_development_dependency "bundler", "~> 1.11"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.0"
end
